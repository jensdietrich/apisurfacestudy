


# Scripts in This Modules

Scripts have to be performed in this order. Scripts consume and produce JSON files. 


## FetchPopularMvnCategoriesAndArtifacts


Fetches popular categories from https://mvnrepository.com/open-source/ , then the top artefacts for each of those categories. The number of category and artefact pages can be configured in the script. Results are written to`results/popular-mvn-categories-and-artifacts.json`.  

## FetchPOMs

Takes `results/popular-mvn-categories-and-artifacts.json` as input, tries to fetch poms, saves them locally and creates a new file `results/artifacts-with-latest-pom.json` with references to the remote and the local pom.

## ExtractSCMInfoFromPOMs 

Takes `results/artifacts-with-latest-pom.json` as input, and tries to locate the repo from `scm` entries in the pom. If no local pom is present, will look for a file `pom-repo-hint.txt` that contains the repo URL (this can be used to manually add repos that cannot be found automatically). Creates the file `results/artifacts-with-latest-pom-and-scm.json`. 

## FetchGitRepos 

Takes `results/artifacts-with-latest-pom-and-scm.json` as input, and clones the repository if it is a git repository. A new file named `results/artifacts-with-local-git-repo.json` is created that contains a reference to the local clone of the repository.

## ExtractTagsFromLocalRepos

Takes `results/artifacts-with-local-git-repo.json`, extracts the tags in the local repos (if there is one and it is git), and adds an attribute with the tag names, the augmented data is written to `results/artifacts-with-tags.json`.

## SelectAndCheckoutTag

Takes `results/artifacts-with-tags.json`, and processes projects with local repos and tags. Selects a tag, and checks out
this tag in the local repo. A `selectedTag` property is added to the project summary, and the updated file
is written to `results/artifacts-with-selected-tag.json`. The heuristics to select a tag is implemented in
`DefaultVersionSelector`, which uses `DefaultVersionComparator`.

## PrintProjectSummary

Prints a summary of information collected by the previous scripts. 


