package nz.ac.wgtn.ecs.apisurfacestudy.dataacquisition.popularmvn;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class VersionParserTest {

    @Test
    public void testParser1() {
        Version version = Version.from("v3.4.5.M1");
        assertEquals(3,version.getMajor());
        assertEquals(4,version.getMinor());
        assertEquals(5,version.getMicro());
        assertEquals(-1,version.getNano());
        assertEquals("v",version.getPrefix());
        assertEquals(".M1",version.getSuffix());
        assertTrue(version.isPrerelease());
    }

    @Test
    public void testParser2() {
        Version version = Version.from("metrics-librato-2.1.2.0");
        assertEquals(2,version.getMajor());
        assertEquals(1,version.getMinor());
        assertEquals(2,version.getMicro());
        assertEquals(0,version.getNano());
        assertEquals("metrics-librato-",version.getPrefix());
        assertEquals("",version.getSuffix());
        assertTrue(!version.isPrerelease());
    }

    @Test
    public void testParser3() {
        Version version = Version.from("ormlite-jdbc-5.1");
        assertEquals(5,version.getMajor());
        assertEquals(1,version.getMinor());
        assertEquals(-1,version.getMicro());
        assertEquals(-1,version.getNano());
        assertEquals("ormlite-jdbc-",version.getPrefix());
        assertEquals("",version.getSuffix());
        assertTrue(!version.isPrerelease());
    }

    @Test
    public void testParser4() {
        Version version = Version.from("Rhino1_7_8_1_RELEASE");
        assertEquals(1,version.getMajor());
        assertEquals(7,version.getMinor());
        assertEquals(8,version.getMicro());
        assertEquals(1,version.getNano());
        assertEquals("Rhino",version.getPrefix());
        assertEquals("_RELEASE",version.getSuffix());
        assertTrue(!version.isPrerelease());
    }

    // test prerelease property
    @Test
    public void testPreRelease1() {
        assertTrue(Version.from("v3.0.0.M1").isPrerelease());
    }

    @Test
    public void testPreRelease2() {
        assertTrue(Version.from("v5.3.0-RC2").isPrerelease());
    }

    @Test
    public void testPreRelease3() {
        assertTrue(Version.from("p6spy-3.0.0-rc1").isPrerelease());
    }

    @Test
    public void testPreRelease4() {
        assertFalse(Version.from("v4.0.0.Magic").isPrerelease());
    }

    @Test
    public void testPreRelease5() {
        assertTrue(Version.from("v3.0.0.Alpha").isPrerelease());
    }

    @Test
    public void testPreRelease6() {
        assertTrue(Version.from("v3.2.0.beta1").isPrerelease());
    }

    @Test
    public void testPreRelease7() {
        assertTrue(Version.from("p6spy-3.0.0-alpha-1").isPrerelease());
    }

    @Test
    public void testPreRelease8() {
        assertTrue(Version.from("v2.0.0-BETA16-with-2.9.1").isPrerelease());
    }

    @Test
    public void testPreRelease9() {
        assertTrue(!Version.from("1.2.3").isPrerelease());
    }

    @Test
    public void testPreRelease10() {
        assertTrue(!Version.from("v1.2.3").isPrerelease());
    }

    @Test
    public void testPreRelease11() {
        assertTrue(!Version.from("1.2.3-final").isPrerelease());
    }

    @Test
    public void testPreRelease12() {
        assertTrue(!Version.from("v1.2.3-FINAL").isPrerelease());
    }
}
