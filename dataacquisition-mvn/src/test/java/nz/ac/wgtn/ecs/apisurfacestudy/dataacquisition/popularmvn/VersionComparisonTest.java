package nz.ac.wgtn.ecs.apisurfacestudy.dataacquisition.popularmvn;


import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class VersionComparisonTest {


    private void assertLessThan(String v1,String v2) {
        Version version1 = Version.from(v1);
        Version version2 = Version.from(v2);
        // assertTrue(0<version2.compareTo(version1));
        assertTrue(0>new DefaultVersionComparator().compare(version1,version2));
    }

    @Test
    public void testMajorRule1a() {
        assertLessThan("1.2.3","2.1.1");
    }

    @Test
    public void testMajorRule1b() {
        assertLessThan("1_2_3","2_1_1");
    }

    @Test
    public void testMajorRule2a() {
        assertLessThan("1.2","2.1");
    }

    @Test
    public void testMajorRule2b() {
        assertLessThan("1_2","2_1");
    }
    
    @Test
    public void testMinorRule1a() {
        assertLessThan("1.1.0","1.2.0");
    }

    @Test
    public void testMinorRule1b() {
        assertLessThan("1.1.0","1.2.0");
    }

    @Test
    public void testMinorRule2a() {
        assertLessThan("1.1","1.2");
    }

    @Test
    public void testMinorRule2b() {
        assertLessThan("1_1","1_2");
    }

    @Test
    public void testMinorRule3a() {
        assertLessThan("1.9","1.10");
    }

    @Test
    public void testMinorRule3b() {
        assertLessThan("1_9","1_10");
    }

    
    @Test
    public void testMinorRule4a()  {
        assertLessThan("1.1","1.10");
    }

    @Test
    public void testMinorRule4b()  {
        assertLessThan("1_1","1_10");
    }

    @Test
    public void testMicroRule1a()  {
        assertLessThan("1.1.0","1.2.0");
    }

    @Test
    public void testMicroRule1b()  {
        assertLessThan("1_1_0","1_2_0");
    }

    @Test
    public void testMicroRule2a()  {
        assertLessThan("1.2.3.1","1.2.4.0");
    }

    @Test
    public void testMicroRule2b() {
        assertLessThan("1_2_3_1","1_2_4_0");
    }

    @Test
    public void testNanoRule2a() {
        assertLessThan("1.2.3.4","1.2.3.5");
    }

    @Test
    public void testNanoRule2b() {
        assertLessThan("1_2_3_4","1_2_3_5");
    }

    @Test
    public void testPrereleaseRule1a() {
        assertLessThan("1.2.3-RC1","1.2.3");
    }

    @Test
    public void testPrereleaseRule1b() {
        assertLessThan("1_2_3-RC1","1_2_3");
    }

    @Test
    public void testPrereleaseRule2a() {
        assertLessThan("1.2.3-M1","1.0.0");
    }

    @Test
    public void testPrereleaseRule2b() {
        assertLessThan("1_2_3-M1","1_0_0");
    }

    @Test
    public void testMisc1() {
        assertLessThan("initial","BCEL_5_0");
    }
}
