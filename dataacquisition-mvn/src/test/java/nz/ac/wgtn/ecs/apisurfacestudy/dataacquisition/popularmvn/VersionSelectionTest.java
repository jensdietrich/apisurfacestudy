package nz.ac.wgtn.ecs.apisurfacestudy.dataacquisition.popularmvn;

import com.google.gson.Gson;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

public class VersionSelectionTest {

    static List<Version> load(String fileName) throws Exception {
        File file = new File(VersionSelectionTest.class.getResource(fileName).getFile());
        Assumptions.assumeTrue(file.exists());
        try (Reader reader = new FileReader(file)) {
            List<String> tags = new Gson().fromJson(reader,List.class);
            return tags.stream().map(tag -> Version.from(tag)).collect(Collectors.toList());
        }
    }

    private Version select(String fileName) throws Exception {
        List<Version> versions = load(fileName);
        return new DefaultVersionSelector().apply(versions);
    }

    @Test
    public void testVersionSelection_scala3lib() throws Exception {
        Version version = select("/scala3-lib-tags.json");
        assertEquals("3.0.0",version.getTag());
    }

    @Test
    public void testVersionSelection_javassist() throws Exception {
        Version version = select("/javassist-tags.json");
        assertEquals("rel_3_28_0_ga",version.getTag());
    }

    @Test
    public void testVersionSelection_asm() throws Exception {
        Version version = select("/asm-tags.json");
        assertEquals("ASM_9_1",version.getTag());
    }

    @Test
    public void testVersionSelection_bcel() throws Exception {
        Version version = select("/bcel-tags.json");
        assertEquals("rel/commons-bcel-6.5.0",version.getTag());
    }

    @Test
    public void testVersionSelection_jcommander() throws Exception {
        Version version = select("/jcommander-tags.json");
        assertEquals("1.81",version.getTag());
    }

    @Test
    public void testVersionSelection_jopt() throws Exception {
        Version version = select("/jopt-tags.json");
        assertEquals("jopt-simple-5.0.4",version.getTag()); // skip 6 betas
    }

    @Test
    public void testVersionSelection_azurecore() throws Exception {
        Version version = select("/azure-core-tags.json");
        assertEquals("azure-storage-common_12.11.1",version.getTag()); // skip 6 betas
    }

}
