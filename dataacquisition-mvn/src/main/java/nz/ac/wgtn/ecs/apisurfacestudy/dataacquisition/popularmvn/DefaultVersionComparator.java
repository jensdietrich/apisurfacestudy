package nz.ac.wgtn.ecs.apisurfacestudy.dataacquisition.popularmvn;

import javax.annotation.Nonnull;
import java.util.Comparator;

/**
 * Default version comparator.
 * Note that this always ranks pre-release versions low.
 * E.g. 1.2.3-RC < 1.0.0
 * @author jens dietrich
 */
public class DefaultVersionComparator implements Comparator<Version> {

    @Override
    public int compare(@Nonnull Version o1, @Nonnull Version o2) {
        int[] k1 = getKeys(o1);
        int[] k2 = getKeys(o2);
        assert k1.length==k2.length;

        for (int i=0;i<k1.length;i++) {
            int diff = k1[i]-k2[i];
            if (diff!=0) {
                return diff;
            }
        }
        return 0;
    }

    @Nonnull private int[] getKeys(Version version) {
        return new int[]{
            version.isPrerelease() ? -1 : 0,
            version.getMajor(),
            version.getMinor(),
            version.getMicro(),
            version.getNano(),
            -version.getSuffix().length()
        };

    }
}


