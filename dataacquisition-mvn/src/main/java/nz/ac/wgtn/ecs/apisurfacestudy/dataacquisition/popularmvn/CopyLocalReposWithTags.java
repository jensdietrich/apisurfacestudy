package nz.ac.wgtn.ecs.apisurfacestudy.dataacquisition.popularmvn;

import com.google.common.base.Preconditions;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Copy all local repos with a selected and checkout tag.
 * @author jens dietrich
 */
public class CopyLocalReposWithTags {

    public static final Logger LOGGER = LogManager.getLogger(SelectAndCheckoutTag.class);


    public static void main (String[] args) throws IOException {

        Preconditions.checkArgument(args.length==1,"One argument required - the name of the destination folder");
        String dir = args[0];
        File root = new File(dir);
        root.mkdirs();

        LOGGER.info("Repos will be copied to " + root.getAbsolutePath());
        File previousResultsFile = new File(SelectAndCheckoutTag.RESULTS);
        Preconditions.checkState(previousResultsFile.exists(), "File " + previousResultsFile + " does not exist, run " + SelectAndCheckoutTag.class.getName() + " first");
        List<Artifact> artifacts = Artifact.loadFrom(previousResultsFile);

        // artifacts = artifacts.stream().limit(1).collect(Collectors.toList()); // selection for debugging

        for (Artifact artifact:artifacts) {
            String localRepoPath = artifact.getLocalRepo();
            if (localRepoPath!=null) {
                File localRepo = new File(localRepoPath);
                String tag = artifact.getSelectedTag();
                if (localRepo.exists() && tag!=null) {
                    File dest = new File(root, artifact.getGroupId());
                    dest = new File(dest,artifact.getArtifactId());
                    dest.mkdirs();
                    try {
                        FileUtils.copyDirectory(localRepo, dest);
                        LOGGER.info("Copied repo for artifact  " + artifact.getName() + " @tag=" + tag);
                    }
                    catch (Exception x) {
                        LOGGER.error("Errors copying repo for artifact  " + artifact.getName(),x);
                    }
                }
            }
        }


    }
}
