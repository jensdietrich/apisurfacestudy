package nz.ac.wgtn.ecs.apisurfacestudy.dataacquisition.popularmvn;

import com.google.common.base.Preconditions;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

/**
 * Default version selector.
 * E.g. 1.2.3-RC < 1.0.0
 * @author jens dietrich
 */
public class DefaultVersionSelector implements Function<List<Version>,Version> {

    @Override
    public Version apply(@Nonnull List<Version> versions) {
        Preconditions.checkArgument(versions!=null);
        Preconditions.checkArgument(!versions.isEmpty());
        List<Version> sortedVersions = new ArrayList<>();
        sortedVersions.addAll(versions);
        Collections.sort(sortedVersions,new DefaultVersionComparator());
        return sortedVersions.get(sortedVersions.size()-1);
    }
}


