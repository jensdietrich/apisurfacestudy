package nz.ac.wgtn.ecs.apisurfacestudy.dataacquisition.popularmvn;

import com.google.common.base.Preconditions;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.lib.Ref;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Extract tags from local repos.
 * @author jens dietrich
 */
public class ExtractTagsFromLocalRepos {

    public static final Logger LOGGER = LogManager.getLogger(ExtractTagsFromLocalRepos.class);
    public static final String RESULTS = "results/artifacts-with-tags.json";

    public static void main (String[] args) throws Exception {
        Preconditions.checkState(!new File(RESULTS).exists(), "Script would override " + RESULTS + " - backup and remove manually first");

        // fetch popular categories and top artifacts within those categories
        File previousResultsFile = new File(FetchGitRepos.RESULTS);
        Preconditions.checkState(previousResultsFile.exists(), "File " + previousResultsFile + " does not exist, run " + FetchGitRepos.class.getName() + " first");

        List<Artifact> artifacts = Artifact.loadFrom(previousResultsFile);

        // apply some filters
        artifacts = artifacts.stream()
            .filter(art -> art.getLocalRepo()!=null)
            .filter(art -> art.isGitRepo())
            //.limit(1) // FOR DEBUGGING
            .collect(Collectors.toList());

        for (Artifact artifact : artifacts) {

            LOGGER.info("extracting tags from: " + artifact.toString());

            File repoDir = new File(artifact.getLocalRepo());
            Preconditions.checkState(repoDir.exists());
            List<String> tags = new ArrayList<>();
            List<Ref> call = Git.open(repoDir).tagList().call();
            for (Ref ref : call) {
                // System.out.println("Tag: " + ref + " " + ref.getName() + " " + ref.getObjectId().getName());
                String name = ref.getName();
                if (name.startsWith("refs/tags/")) {
                    name = name.substring("refs/tags/".length());
                    LOGGER.info("\ttag extracted and added: "+name);
                    tags.add(name);
                }
                else {
                    LOGGER.warn("\t illegal tag ref encountered: "+name);
                }
            }
            artifact.setTags(tags);
        }

        // export results
        Artifact.saveTo(artifacts,new File(RESULTS));
        LOGGER.info("Tags gathered and written to " + RESULTS);
    }

}
