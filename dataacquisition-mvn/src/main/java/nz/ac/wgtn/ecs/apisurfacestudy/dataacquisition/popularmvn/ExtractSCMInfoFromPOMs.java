package nz.ac.wgtn.ecs.apisurfacestudy.dataacquisition.popularmvn;

import com.google.common.base.Preconditions;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXParseException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Path;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;

/**
 * Extract scm info from POMs.
 * @author jens dietrich
 */
public class ExtractSCMInfoFromPOMs {


    public static final Logger LOGGER = LogManager.getLogger(ExtractSCMInfoFromPOMs.class);
    public static final String RESULTS = "results/artifacts-with-latest-pom-and-scm.json";

    public static void main(String[] args) throws Exception {

        Preconditions.checkState(!new File(RESULTS).exists(),"Script would override " + RESULTS + " - backup and remove manually first");

        // fetch popular categories and top artifacts within those categories
        File previousResultsFile = new File(FetchPOMs.RESULTS);
        Preconditions.checkState(previousResultsFile.exists(), "File " + previousResultsFile + " does not exist, run " + FetchPOMs.class.getName() + " first");

        List<Artifact> artifacts = Artifact.loadFrom(previousResultsFile);

        // limit no for for DEBUGGING !
        // artifacts = artifacts.stream().limit(3).collect(Collectors.toList());

        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = builderFactory.newDocumentBuilder();

        // process records
        int scmConnectionCounter = 0;
        int scmURLCounter = 0;
        for (Artifact artifact : artifacts) {
            if (artifact.getLatestVersionPOMLocalFile() == null) {
                fetchPomFromHint(artifact);
            }
            if (artifact.getLatestVersionPOMLocalFile() != null) {
                File pom = new File(artifact.getLatestVersionPOMLocalFile());
                if (pom.exists()) {
                    try {
                        Document xmlDocument = builder.parse(new FileInputStream(pom));
                        XPath xPath = XPathFactory.newInstance().newXPath();
                        String expression = "/project/scm/connection";
                        NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODESET);
                        if (nodeList.getLength() == 1) {
                            String value = nodeList.item(0).getTextContent();
                            artifact.setScmConnection(value);
                            scmConnectionCounter = scmConnectionCounter + 1;
                            artifact.getComments().add("scm connection extracted from pom by " + ExtractSCMInfoFromPOMs.class.getName() + " on " + DateFormat.getDateTimeInstance().format(new Date()));
                        }

                        expression = "/project/scm/url";
                        nodeList = (NodeList) xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODESET);
                        if (nodeList.getLength() == 1) {
                            String value = nodeList.item(0).getTextContent();
                            artifact.setScmURL(value);
                            scmURLCounter = scmURLCounter + 1;
                            artifact.getComments().add("scm URL extracted from pom by " + ExtractSCMInfoFromPOMs.class.getName() + " on " + DateFormat.getDateTimeInstance().format(new Date()));
                        }
                    }
                    catch (SAXParseException x) {
                        LOGGER.warn("Error parsing pom " + pom.toPath().toString(),x);
                    }
                } else {
                    LOGGER.info("No pom found, skipping: " + artifact.getArtifactURL());
                    artifact.getComments().add("No scm info found in pom, cannot extract scm with " + ExtractSCMInfoFromPOMs.class.getName());
                }
            } else {
                LOGGER.info("No pom found, skipping: " + artifact.getArtifactURL());
                artifact.getComments().add("No scm info found in pom, cannot extract scm with " + ExtractSCMInfoFromPOMs.class.getName());
            }
        }

        LOGGER.info("artifacts analysed: " + artifacts.size());
        LOGGER.info("scm connections set: " + scmConnectionCounter);
        LOGGER.info("scm URLs set: " + scmURLCounter);

        // export results
        Artifact.saveTo(artifacts,new File(RESULTS));
        LOGGER.info("Artifact urls gathered written to " + RESULTS);
    }

    private static void fetchPomFromHint(Artifact artifact) throws Exception {
        // try to fetch from hint
        HttpClient client = HttpClient.newHttpClient();
        File artifactFolder = new File(new File(FetchPOMs.POM_FOLDER),artifact.getGroupId()+"/"+artifact.getArtifactId());
        File pomHint = new File(artifactFolder,"pom-repo-hint.txt");
        File pomFile = new File(artifactFolder,"pom.xml");
        if (pomHint.exists()) {
            try (BufferedReader reader = new BufferedReader(new FileReader(pomHint))) {
                String url = reader.readLine();
                HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create(url))
                    .build();
                HttpResponse<Path> response = client.send(request, HttpResponse.BodyHandlers.ofFile(pomFile.toPath()));
                if (response.statusCode() >= 400) {
                    pomFile.delete();
                    LOGGER.warn("Error fetching pom from hint with get " + url + " status is " + response.statusCode());
                } else {
                    artifact.setLatestVersionPOMURL(url);
                    artifact.setLatestVersionPOMLocalFile(pomFile.toPath().toString());
                    artifact.getComments().add("pom fetched by " + ExtractSCMInfoFromPOMs.class.getName() + " using hint on " + DateFormat.getDateTimeInstance().format(new Date()));
                }
            }
        }
    }
}
