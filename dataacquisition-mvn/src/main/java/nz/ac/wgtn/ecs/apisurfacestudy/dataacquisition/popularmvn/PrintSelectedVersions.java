package nz.ac.wgtn.ecs.apisurfacestudy.dataacquisition.popularmvn;

import com.google.common.base.Preconditions;
import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Adhoc script to print the selected version (tag) for each artifact with a local git repo.
 * @author jens dietrich
 */
public class PrintSelectedVersions {

    public static void main (String[] args) throws Exception {

        File input = new File(ExtractTagsFromLocalRepos.RESULTS);
        System.out.println(input.getAbsolutePath());
        Preconditions.checkState(input.exists(), "File " + input.getPath().toString() + " does not exist, run " + ExtractTagsFromLocalRepos.class.getName() + " first");

        List<Artifact> artifacts = Artifact.loadFrom(input);

        for (Artifact artifact:artifacts) {
            if (artifact.getTags()!=null && artifact.getTags().size()>0) {
                System.out.println(artifact.getGroupId() + "/" + artifact.getArtifactId());
                System.out.println("\ttags available: " + artifact.getTags().size());
                List<Version> versions = artifact.getTags()
                    .stream()
                    .map(tag -> Version.from(tag))
                    .collect(Collectors.toList());
                Version selectedVersion = new DefaultVersionSelector().apply(versions);
                System.out.println("\ttag selected: " + selectedVersion.getTag());
            }
        }
    }
}
