package nz.ac.wgtn.ecs.apisurfacestudy.dataacquisition.popularmvn;

import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.Comparator.comparing;

/**
 * Print projects with a SCM connection by category.
 * @author jens dietrich
 */
public class PrintProjectSummary {

    public static final Logger LOGGER = LogManager.getLogger(PrintProjectSummary.class);

    // in reverse order, richest / latest first
    public static final File[] RESULTS = {
        new File(FetchGitRepos.RESULTS),
        new File(ExtractSCMInfoFromPOMs.RESULTS),
        new File(FetchPOMs.RESULTS),
        new File(FetchPopularMvnCategoriesAndArtifacts.RESULTS)
    };

    public static void main(String[] args) throws Exception {

        List<Artifact> artifacts = null;
        for (File data:RESULTS) {
            if (data.exists()) {
                artifacts = Artifact.loadFrom(data);
                LOGGER.info("Using artifact data from " + data.toPath().toString());
                break;
            }
        }
        Preconditions.checkState(artifacts!=null);


        artifacts.stream()
            // .filter(artifact -> artifact.getScmConnection()!=null)
            .sorted(
                 comparing(Artifact::getCategory).thenComparing(comparing(Artifact::getRankInCategory))
            )
            .forEach(artifact ->
                LOGGER.info(artifact.getCategory()+" - " + artifact.getRankInCategory() + " : " + artifact.getArtifactURL() + (artifact.getScmConnection()==null?" - NO SCM ! ":""))
            );

        LOGGER.info("=== SUMMARY ===");
        LOGGER.info("artifacts: "+ artifacts.size());
        LOGGER.info("categories: " + artifacts.stream().map(artifact -> artifact.getCategory()).collect(Collectors.toSet()).size());
        LOGGER.info("artifacts with scm connection: "+ artifacts.stream().filter(artifact -> artifact.getScmConnection()!=null).count());
        LOGGER.info("artifacts with local repos: "+ artifacts.stream().filter(artifact -> artifact.getLocalRepo()!=null).count());
        LOGGER.info("artifacts with local repos and maven: "+ artifacts.stream().filter(artifact -> artifact.isMavenProject()).count());
        LOGGER.info("artifacts with local repos and gradle (groovy DSL): "+ artifacts.stream().filter(artifact -> artifact.isGradleProject()).count());
        LOGGER.info("artifacts with local repos and gradle (kotlin DSL): "+ artifacts.stream().filter(artifact -> artifact.isGradleKotlinProject()).count());
        LOGGER.info("artifacts with local repos and sbt: "+ artifacts.stream().filter(artifact -> artifact.isSbtProject()).count());
    }

}
