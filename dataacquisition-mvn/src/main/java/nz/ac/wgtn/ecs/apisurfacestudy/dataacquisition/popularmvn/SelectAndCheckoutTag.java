package nz.ac.wgtn.ecs.apisurfacestudy.dataacquisition.popularmvn;

import com.google.common.base.Preconditions;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jgit.api.Git;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Select a tag based on the heuristics implemented in DefaultVersionSelector , and checkout the respective tag.
 * @author jens dietrich
 */
public class SelectAndCheckoutTag {

    public static final Logger LOGGER = LogManager.getLogger(SelectAndCheckoutTag.class);
    public static final String RESULTS = "results/artifacts-with-selected-tag.json";


    public static void main (String[] args) throws IOException {
        Preconditions.checkState(!new File(RESULTS).exists(), "Script would override " + RESULTS + " - backup and remove manually first");

        // fetch popular categories and top artifacts within those categories
        File previousResultsFile = new File(ExtractTagsFromLocalRepos.RESULTS);
        Preconditions.checkState(previousResultsFile.exists(), "File " + previousResultsFile + " does not exist, run " + ExtractTagsFromLocalRepos.class.getName() + " first");

        List<Artifact> artifacts = Artifact.loadFrom(previousResultsFile);

        // select tag
        LOGGER.info("Selecting tags");
        for (Artifact artifact:artifacts) {
            List<String> tags = artifact.getTags();
            List<Version> versions = tags.stream().map(tag -> Version.from(tag)).collect(Collectors.toList());
            if (versions!=null && versions.size()>0) {
                Version selectedVersion = new DefaultVersionSelector().apply(versions);
                artifact.setSelectedTag(selectedVersion.getTag());
            }
        }

        // export results
        Artifact.saveTo(artifacts,new File(RESULTS));
        LOGGER.info("Tag selection and written to " + RESULTS);

        // checkout tag
        for (Artifact artifact:artifacts) {
            File repoDir = new File(artifact.getLocalRepo());
            String tag = artifact.getSelectedTag();
            if (repoDir!=null && repoDir.exists() && tag!=null) {
                try {
                    Git git = Git.open(repoDir);
                    git.checkout().setName("refs/tags/" + tag).call();
                    LOGGER.info("Checked out tag " + tag + " for " + artifact.getName());
                }
                catch (Exception x) {
                    LOGGER.error("Problems checking out tag " + tag + " for " + artifact.getName(),x);

                }
            }
        }
    }
}
