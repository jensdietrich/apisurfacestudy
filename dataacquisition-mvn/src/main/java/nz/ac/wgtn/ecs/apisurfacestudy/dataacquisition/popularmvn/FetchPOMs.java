package nz.ac.wgtn.ecs.apisurfacestudy.dataacquisition.popularmvn;

import com.google.common.base.Preconditions;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Path;
import java.text.DateFormat;
import java.util.*;

/**
 * Fetch poms of popular maven artifacts from maven.
 * @author jens dietrich
 */
public class FetchPOMs {

    public static final Logger LOGGER = LogManager.getLogger(FetchPOMs.class);
    public static final String POM_FOLDER = "results/poms";
    public static final String RESULTS = "results/artifacts-with-latest-pom.json";

    public static void main(String[] args) throws Exception {

        Preconditions.checkState(!new File(RESULTS).exists(),"Script would override " + RESULTS + " - backup and remove manually first");

        // fetch popular categories and top artifacts within those categories
        File previousResultsFile = new File(FetchPopularMvnCategoriesAndArtifacts.RESULTS);
        Preconditions.checkState(previousResultsFile.exists(), "File " + previousResultsFile + " does not exist, run " + FetchPopularMvnCategoriesAndArtifacts.class.getName() + " first");
        List<Artifact> artifacts = Artifact.loadFrom(previousResultsFile);

        HttpClient client = HttpClient.newHttpClient();
        File pomRootFolder = new File(POM_FOLDER);

        // limit no for for DEBUGGING !
        // artifacts = artifacts.stream().limit(3).collect(Collectors.toList());

        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = builderFactory.newDocumentBuilder();

        // process records
        for (Artifact artifact:artifacts) {
            File pomFolder = new File(pomRootFolder,artifact.getGroupId()+"/"+artifact.getArtifactId());
            pomFolder.mkdirs();

            // fetch metadata
            String metaURL = "https://repo1.maven.org/maven2/" + artifact.getGroupId().replace('.','/') + "/" + artifact.getArtifactId() + "/maven-metadata.xml" ;
            artifact.setMetadataURL(metaURL);
            LOGGER.info("fetching metadata from " + metaURL);
            File metaDataFile = new File(pomFolder,"maven-metadata.xml");
            if (metaDataFile.exists()) {
                LOGGER.info("metadata for artifact " + artifact.getArtifactURL() + " has already been acquired: " + metaDataFile.getAbsolutePath());
            }
            else {
                HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create(metaURL))
                    .build();
                HttpResponse<Path> response = client.send(request, HttpResponse.BodyHandlers.ofFile(metaDataFile.toPath()));
                if (response.statusCode() == 200) {
                    LOGGER.info("Response status: " + response.statusCode());
                    artifact.setMetadataURL(metaURL);
                    artifact.setMetadataLocalFile(metaDataFile.toPath().toString());
                    artifact.getComments().add("version metadata collected by " + FetchPOMs.class.getName() + " on " + DateFormat.getDateTimeInstance().format(new Date()));
                } else if (response.statusCode() >= 400) {  // usually 404
                    LOGGER.error("Response status: " + response.statusCode());
                    LOGGER.error("\tfailed to fetch metadata for artifact: " + artifact.getArtifactURL());
                    LOGGER.error("\tfrom: " + metaURL);
                    artifact.setMetadataURL(metaURL);
                    // the response handler will still write a (html) file with error info, so we will delete this to keep state consistent
                    metaDataFile.delete();
                    artifact.getComments().add("problem collecting metadata with " + FetchPOMs.class.getName() + " on " + DateFormat.getDateTimeInstance().format(new Date()) + "\tGET " + metaURL + " returned " + response.statusCode());
                } else {
                    LOGGER.warn("Response status: " + response.statusCode());
                    LOGGER.error("\tmanually check " + metaURL);
                    artifact.setMetadataURL(metaURL);
                    artifact.setMetadataLocalFile(metaDataFile.toPath().toString());
                    artifact.getComments().add("issue collecting metadata with " + FetchPOMs.class.getName() + " on " + DateFormat.getDateTimeInstance().format(new Date()) + "\tGET " + metaURL + " returned " + response.statusCode());
                }
            }

            // fetch latest version from metadata
            if (metaDataFile.exists()) {

                Document xmlDocument = builder.parse(new FileInputStream(metaDataFile));
                XPath xPath = XPathFactory.newInstance().newXPath();
                String expression = "/metadata/versioning/latest";
                NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODESET);
                if (nodeList.getLength()==1) {
                    String latestVersion = nodeList.item(0).getTextContent();
                    artifact.setLatestVersion(latestVersion);
                    artifact.getComments().add("latest version extracted from metadata with " + FetchPOMs.class.getName() + " on " + DateFormat.getDateTimeInstance().format(new Date()));
                }
            }
            String latestVersion = artifact.getLatestVersion();
            if (latestVersion!=null) {
                String pomURL = artifact.getMetadataURL().replace("maven-metadata.xml",latestVersion)+"/"+artifact.getArtifactId()+"-"+latestVersion+".pom";
                File pomFile = new File(pomFolder,"pom.xml");
                if (pomFile.exists()) {
                    LOGGER.info("pom for artifact " + artifact.getArtifactURL() + " has already been acquired: " + pomFile.getAbsolutePath());
                }
                else {
                    HttpRequest request = HttpRequest.newBuilder()
                        .uri(URI.create(pomURL))
                        .build();
                    HttpResponse<Path> response = client.send(request, HttpResponse.BodyHandlers.ofFile(pomFile.toPath()));
                    if (response.statusCode() == 200) {
                        LOGGER.info("Response status: " + response.statusCode());
                        artifact.setLatestVersionPOMURL(pomURL);
                        artifact.setLatestVersionPOMLocalFile(pomFile.toPath().toString());
                        artifact.getComments().add("latest pom collected by " + FetchPOMs.class.getName() + " on " + DateFormat.getDateTimeInstance().format(new Date()));
                    } else if (response.statusCode() >= 400) {  // usually 404
                        LOGGER.error("Response status: " + response.statusCode());
                        LOGGER.error("\tfailed to fetch pom for artifact: " + artifact.getArtifactURL());
                        LOGGER.error("\tfrom: " + metaURL);
                        // the response handler will still write a (html) file with error info, so we will delete this to keep state consistent
                        pomFile.delete();
                        artifact.getComments().add("problem collecting pom with " + FetchPOMs.class.getName() + " on " + DateFormat.getDateTimeInstance().format(new Date()) + "\tGET " + pomURL + " returned " + response.statusCode());
                    } else {
                        LOGGER.warn("Response status: " + response.statusCode());
                        LOGGER.error("\tmanually check " + pomURL);
                        artifact.setLatestVersionPOMURL(pomURL);
                        artifact.setLatestVersionPOMLocalFile(pomFile.toPath().toString());
                        artifact.getComments().add("issue collecting pom with " + FetchPOMs.class.getName() + " on " + DateFormat.getDateTimeInstance().format(new Date()) + "\tGET " + pomURL + " returned " + response.statusCode());
                    }
                }
            }

        }

        // export results
        Artifact.saveTo(artifacts,new File(RESULTS));
        LOGGER.info("Updated results saved to " + new File(RESULTS).toPath().toString());
    }


}
