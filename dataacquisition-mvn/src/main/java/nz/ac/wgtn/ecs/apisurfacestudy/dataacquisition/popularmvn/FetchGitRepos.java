package nz.ac.wgtn.ecs.apisurfacestudy.dataacquisition.popularmvn;

import com.google.common.base.Preconditions;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jgit.api.Git;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Script to fetch the git repo using the scmConnection attribute extracted from POMs.
 * @author jens dietrich
 */
public class FetchGitRepos {

    public static final Logger LOGGER = LogManager.getLogger(FetchGitRepos.class);
    public static final String RESULTS = "results/artifacts-with-local-git-repo.json";
    public static final String REPO_ROOT = "results/git-repos";

    public static void main (String[] args) throws Exception {

        File previousResultsFile = new File(ExtractSCMInfoFromPOMs.RESULTS);
        File resultFile = new File(RESULTS);
        Preconditions.checkState(previousResultsFile.exists() || resultFile.exists(), "Neither " + previousResultsFile.toPath().toString() + " nor " + resultFile.toPath().toString() + " exist exist, run " + ExtractSCMInfoFromPOMs.class.getName() + " first");

        List<Artifact> artifacts = null;
        if (resultFile.exists()){
            artifacts = Artifact.loadFrom(resultFile);
            LOGGER.info("Artifacts loaded from " + resultFile.toPath().toString());
        }
        else if (previousResultsFile.exists()) {
            artifacts = Artifact.loadFrom(previousResultsFile);
            LOGGER.info("Artifacts loaded from " + previousResultsFile.toPath().toString());
        }

        // for DEBUGGING
        // artifacts = artifacts.stream().filter(artifact -> artifact.isGitRepo()).limit(1).collect(Collectors.toList());

        // prep folder
        File repoRoot = new File(REPO_ROOT);
        repoRoot.mkdirs();

        artifacts.stream()
            .filter(artifact -> artifact.isGitRepo())
            .forEach(artifact ->
            {
                assert artifact.getScmConnection()!=null;
                assert artifact.getScmConnection().startsWith("scm:git");

                String repoURI = artifact.getScmURL();
                if (repoURI==null) {
                    // avoid SSH setup, switch protocol to HTTPS
                    repoURI = artifact.getScmConnection().replace("scm:git:", "");
                    if (!repoURI.startsWith("https://")) {
                        repoURI = repoURI.replace("git@", "https://");
                        repoURI = repoURI.replace(".com:", ".com/");
                        repoURI = repoURI.replace("ssh://", "");
                        repoURI = repoURI.replace("scm:", "");
                    }
                }
                File localRepo = new File(repoRoot,artifact.getGroupId()+'/'+artifact.getArtifactId());

                if (localRepo.exists()) {
                    LOGGER.info("Skipping fetching repo from " + repoURI + " into existing folder " + localRepo);
                }
                else {
                    assert artifact.getLocalRepo()==null;
                    // clone repo
                    try {
                        LOGGER.info("Fetching repo from " + artifact.getScmConnection());
                        LOGGER.info("\tusing: " + repoURI);
                        LOGGER.info("\tcloning into: " + localRepo.toPath().toString());
                        Git.cloneRepository()
                            .setURI(repoURI)
                            .setDirectory(localRepo)
                            .call();
                        artifact.setLocalRepo(localRepo.toPath().toString());
                    } catch (Exception x) {
                        LOGGER.error("Error fetching repo from " + repoURI, x);
                        try {
                            FileUtils.deleteDirectory(localRepo);
                        } catch (IOException e) {
                            LOGGER.error("Error rolling back (deleting) local dir after failed git clone: " + localRepo.toPath().toString(), e);
                        }
                    }
                }
            });

        // export results
        Artifact.saveTo(artifacts,resultFile);
        LOGGER.info("Updated results saved to " + new File(RESULTS).toPath().toString());
    }
}
