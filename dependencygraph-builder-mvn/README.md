# Dependency Graph Builder

This module contains utilities to extract dependency graphs from maven projects. 
This is done by running `mvn dependency:tree` programmatically, and parsing its output. 

## Extracting the Dependency Tree

This functionality is provided by `nz.ac.wgtn.ecs.apisurfacestudy.dependencies.mvn.DependencyGraphBuilder::parse`, this takes console output 
and produces a graph represented as an instance of `org.jgrapht.Graph`, the argument is a parser listener, the two
implementations available (`UnversionedArtifactDependenciesBuilder` and `VersionedArtifactDependenciesBuilder`) produce 
slightly different graphs with artifacts being represented with and without version. 
The edges represent dependencies, and are labelled with a scope (such as `compile` or `test`).

## Persisting the Dependency Tree

`nz.ac.wgtn.ecs.apisurfacestudy.dependencies.mvn.DependencyGraphPersistencyManager` has methods to export and import graphs
using JSON. 

`nz.ac.wgtn.ecs.apisurfacestudy.dependencies.mvn.DependencyGraph2Dot` can be used to export a graph to the dot format,
which can be used as import for several graph visualisation tools like [graphviz](https://graphviz.org/). 