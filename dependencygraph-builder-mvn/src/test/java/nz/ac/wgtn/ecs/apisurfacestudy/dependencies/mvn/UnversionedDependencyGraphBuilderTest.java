package nz.ac.wgtn.ecs.apisurfacestudy.dependencies.mvn;

import com.google.common.io.Files;
import org.jgrapht.Graph;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class UnversionedDependencyGraphBuilderTest {

    private List<String> dependencyTreeConsoleOutput = null;

    @BeforeEach
    public void setup () throws IOException {
        File testData = new File(UnversionedDependencyGraphBuilderTest.class.getResource("/dependency-tree-console-output1.txt").getFile());
        this.dependencyTreeConsoleOutput = Files.readLines(testData, Charset.defaultCharset());
    }

    @AfterEach
    public void tearDown () {
        this.dependencyTreeConsoleOutput = null;
    }

    private Artifact parseArtifact(String def) {
        String[] parts = def.split(":");
        return new Artifact(parts[0],parts[1]);
    }

    private String parseScope(String def) {
        String[] parts = def.split(":");
        return parts[4];
    }

    private void assertEdge(Graph<Artifact,Dependency> graph,String src,String dest) {
        Artifact source = parseArtifact(src);
        Artifact sink = parseArtifact(dest);
        String scope = parseScope(dest);
        assertNotNull(source);
        assertNotNull(sink);
        assertTrue(graph.containsVertex(source));
        assertTrue(graph.containsVertex(sink));
        assertTrue(graph.containsEdge(source,sink));
        assertTrue(graph.containsEdge(new Dependency(source.getId(),sink.getId(),scope)));
    }

    private void assertRoot(Graph<Artifact,Dependency> graph,String def,int directDependencyCount) {
        Artifact artifact = parseArtifact(def);
        assertNotNull(artifact);
        assertTrue(graph.containsVertex(artifact));
        assertSame(0,graph.inDegreeOf(artifact));
        assertSame(directDependencyCount,graph.outDegreeOf(artifact));
    }

    @Test
    public void testUnversionedArtifactDependenciesBuilder () {
        UnversionedArtifactDependenciesBuilder listener = new UnversionedArtifactDependenciesBuilder();
        Graph<Artifact,Dependency> graph = DependencyGraphBuilder.parse(dependencyTreeConsoleOutput,listener);

        assertRoot(graph,"nz.ac.wgtn.ecs.apisurfacestudy:dependencygraph-builder-mvn:jar:0.0.1",7);

        assertEdge(graph,"nz.ac.wgtn.ecs.apisurfacestudy:dependencygraph-builder-mvn:jar:0.0.1","org.zeroturnaround:zt-exec:jar:1.12:compile");
        assertEdge(graph,"nz.ac.wgtn.ecs.apisurfacestudy:dependencygraph-builder-mvn:jar:0.0.1","org.slf4j:slf4j-api:jar:1.7.30:compile");
        assertEdge(graph,"nz.ac.wgtn.ecs.apisurfacestudy:dependencygraph-builder-mvn:jar:0.0.1","org.apache.logging.log4j:log4j-api:jar:2.7:compile");
        assertEdge(graph,"nz.ac.wgtn.ecs.apisurfacestudy:dependencygraph-builder-mvn:jar:0.0.1","org.apache.logging.log4j:log4j-core:jar:2.7:compile");
        assertEdge(graph,"nz.ac.wgtn.ecs.apisurfacestudy:dependencygraph-builder-mvn:jar:0.0.1","org.apache.logging.log4j:log4j-slf4j-impl:jar:2.7:compile");
        assertEdge(graph,"nz.ac.wgtn.ecs.apisurfacestudy:dependencygraph-builder-mvn:jar:0.0.1","org.junit.jupiter:junit-jupiter-engine:jar:5.1.0:test");
            assertEdge(graph,"org.junit.jupiter:junit-jupiter-engine:jar:5.1.0:test","org.apiguardian:apiguardian-api:jar:1.0.0:test");
            assertEdge(graph,"org.junit.jupiter:junit-jupiter-engine:jar:5.1.0:test","org.junit.platform:junit-platform-engine:jar:1.1.0:test");
                assertEdge(graph,"org.junit.platform:junit-platform-engine:jar:1.1.0:test","org.junit.platform:junit-platform-commons:jar:1.1.0:test");
                assertEdge(graph,"org.junit.platform:junit-platform-engine:jar:1.1.0:test","org.opentest4j:opentest4j:jar:1.0.0:test");
            assertEdge(graph,"org.junit.jupiter:junit-jupiter-engine:jar:5.1.0:test","org.junit.jupiter:junit-jupiter-api:jar:5.1.0:test");
        assertEdge(graph,"nz.ac.wgtn.ecs.apisurfacestudy:dependencygraph-builder-mvn:jar:0.0.1","com.google.guava:guava:jar:30.1.1-jre:compile");
            assertEdge(graph,"com.google.guava:guava:jar:30.1.1-jre:compile","com.google.guava:failureaccess:jar:1.0.1:compile");
            assertEdge(graph,"com.google.guava:guava:jar:30.1.1-jre:compile","com.google.guava:listenablefuture:jar:9999.0-empty-to-avoid-conflict-with-guava:compile");
            assertEdge(graph,"com.google.guava:guava:jar:30.1.1-jre:compile","com.google.code.findbugs:jsr305:jar:3.0.2:compile");
            assertEdge(graph,"com.google.guava:guava:jar:30.1.1-jre:compile","org.checkerframework:checker-qual:jar:3.8.0:compile");
            assertEdge(graph,"com.google.guava:guava:jar:30.1.1-jre:compile","com.google.errorprone:error_prone_annotations:jar:2.5.1:compile");
            assertEdge(graph,"com.google.guava:guava:jar:30.1.1-jre:compile","com.google.j2objc:j2objc-annotations:jar:1.3:compile");





    }
}
