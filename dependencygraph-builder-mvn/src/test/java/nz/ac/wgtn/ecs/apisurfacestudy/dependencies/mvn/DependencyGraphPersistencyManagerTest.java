package nz.ac.wgtn.ecs.apisurfacestudy.dependencies.mvn;

import com.google.common.io.Files;
import org.jgrapht.Graph;
import org.junit.jupiter.api.Test;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DependencyGraphPersistencyManagerTest {

    @Test
    public void testUnversionedDependencyGraph() throws IOException {
        File testData = new File(DependencyGraphPersistencyManagerTest.class.getResource("/dependency-tree-console-output1.txt").getFile());
        List<String> lines = Files.readLines(testData, Charset.defaultCharset());
        UnversionedArtifactDependenciesBuilder listener = new UnversionedArtifactDependenciesBuilder();
        Graph<Artifact,Dependency> graph = DependencyGraphBuilder.parse(lines,listener);

        File file = new File("graph-unversioned.json");

        DependencyGraphPersistencyManager persistencyManager = new DependencyGraphPersistencyManager();
        persistencyManager.exportDependencyGraph(graph,file);

        Graph<Artifact,Dependency> graph2 = persistencyManager.importDependencyGraph(file);

        assertEquals(graph.vertexSet(),graph2.vertexSet());
        assertEquals(graph.edgeSet(),graph2.edgeSet());

        for (Artifact artifact:graph.vertexSet()) {
            Optional<Artifact> _artifact2 = graph2.vertexSet().stream().filter(a -> a.equals(artifact)).findAny();
            assertTrue(_artifact2.isPresent());
            Artifact artifact2 = _artifact2.get();
            Set<Dependency> dependencies = graph.outgoingEdgesOf(artifact);
            Set<Dependency> dependencies2 = graph2.outgoingEdgesOf(artifact2);
            assertEquals(dependencies,dependencies2);
        }
    }

    @Test
    public void testVersionedDependencyGraph() throws IOException {
        File testData = new File(DependencyGraphPersistencyManagerTest.class.getResource("/dependency-tree-console-output1.txt").getFile());
        List<String> lines = Files.readLines(testData, Charset.defaultCharset());
        VersionedArtifactDependenciesBuilder listener = new VersionedArtifactDependenciesBuilder();
        Graph<VersionedArtifact,Dependency> graph = DependencyGraphBuilder.parse(lines,listener);

        File file = new File("graph-versioned.json");

        DependencyGraphPersistencyManager persistencyManager = new DependencyGraphPersistencyManager();
        persistencyManager.exportDependencyGraph(graph,file);

        Graph<VersionedArtifact,Dependency> graph2 = persistencyManager.importDependencyGraph(file);

        assertEquals(graph.vertexSet(),graph2.vertexSet());
        assertEquals(graph.edgeSet(),graph2.edgeSet());

        for (VersionedArtifact artifact:graph.vertexSet()) {
            Optional<VersionedArtifact> _artifact2 = graph2.vertexSet().stream().filter(a -> a.equals(artifact)).findAny();
            assertTrue(_artifact2.isPresent());
            VersionedArtifact artifact2 = _artifact2.get();
            Set<Dependency> dependencies = graph.outgoingEdgesOf(artifact);
            Set<Dependency> dependencies2 = graph2.outgoingEdgesOf(artifact2);
            assertEquals(dependencies,dependencies2);
        }
    }


}
