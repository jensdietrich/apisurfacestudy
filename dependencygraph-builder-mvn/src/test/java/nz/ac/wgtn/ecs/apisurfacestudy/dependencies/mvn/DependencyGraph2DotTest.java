package nz.ac.wgtn.ecs.apisurfacestudy.dependencies.mvn;

import com.google.common.io.Files;
import org.jgrapht.Graph;
import org.junit.jupiter.api.Test;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

public class DependencyGraph2DotTest {

    @Test
    public void testUnversionedDependencyGraph() throws IOException {
        File testData = new File(DependencyGraphPersistencyManagerTest.class.getResource("/dependency-tree-console-output1.txt").getFile());
        List<String> lines = Files.readLines(testData, Charset.defaultCharset());
        UnversionedArtifactDependenciesBuilder listener = new UnversionedArtifactDependenciesBuilder();
        Graph<Artifact,Dependency> graph = DependencyGraphBuilder.parse(lines,listener);

        File file = new File("graph-unversioned.dot");
        DependencyGraph2Dot.exportDependencyGraph(graph,file,true);

        // only tests that this does not throw an exception !
        // TODO: parse back and compare graph, can use DotImporter

    }

    @Test
    public void testVersionedDependencyGraph() throws IOException {
        File testData = new File(DependencyGraphPersistencyManagerTest.class.getResource("/dependency-tree-console-output1.txt").getFile());
        List<String> lines = Files.readLines(testData, Charset.defaultCharset());
        VersionedArtifactDependenciesBuilder listener = new VersionedArtifactDependenciesBuilder();
        Graph<VersionedArtifact,Dependency> graph = DependencyGraphBuilder.parse(lines,listener);

        File file = new File("graph-versioned.dot");
        DependencyGraph2Dot.exportDependencyGraph(graph,file,true);

        // only tests that this does not throw an exception !
        // TODO: parse back and compare graph, can use DotImporte

    }

}
