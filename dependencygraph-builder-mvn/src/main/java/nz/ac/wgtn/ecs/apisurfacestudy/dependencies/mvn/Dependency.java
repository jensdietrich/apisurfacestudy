package nz.ac.wgtn.ecs.apisurfacestudy.dependencies.mvn;

import org.jgrapht.nio.Attribute;
import org.jgrapht.nio.DefaultAttribute;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Minimalistic representation of a dependency.
 * Source and target references are used to manage equality.
 * @author jens dietrich
 */
public class Dependency{
    private String scope = null;
    private String sourceId = null;
    private String destinationId = null;

    public Dependency() {}

    public Dependency(String sourceId, String destinationId, String scope) {
        this.scope = scope;
        this.sourceId = sourceId;
        this.destinationId = destinationId;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public String getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(String destinationId) {
        this.destinationId = destinationId;
    }

    // persistency
    public Map<String, Attribute> getAttributes() {
        Map<String, Attribute> attributes = new HashMap<>();
        attributes.put("scope", DefaultAttribute.createAttribute(this.scope));
        attributes.put("sourceId", DefaultAttribute.createAttribute(this.sourceId));
        attributes.put("destinationId", DefaultAttribute.createAttribute(this.destinationId));
        return attributes;
    }
    public String getId() {
        return this.sourceId + "--" + this.scope + "->" + this.destinationId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Dependency that = (Dependency) o;
        return Objects.equals(scope, that.scope) &&
                Objects.equals(sourceId, that.sourceId) &&
                Objects.equals(destinationId, that.destinationId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(scope, sourceId, destinationId);
    }
}
