package nz.ac.wgtn.ecs.apisurfacestudy.dependencies.mvn;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.nio.json.JSONExporter;
import org.jgrapht.nio.json.JSONImporter;
import java.io.File;
import java.io.Reader;
import java.io.Writer;

/**
 * Utility to export and import dependency graphs.
 * @author jens dietrich
 */
public class DependencyGraphPersistencyManager<T extends Artifact> {



    private JSONExporter<T,Dependency> exporter = null;
    private JSONImporter<T,Dependency> importer = null;

    public DependencyGraphPersistencyManager() {
        this.exporter = new JSONExporter<>();
        this.exporter.setVertexAttributeProvider(artifact -> artifact.getAttributes());
        this.exporter.setVertexIdProvider(artifact -> artifact.getId());
        this.exporter.setEdgeAttributeProvider(dependency -> dependency.getAttributes());
        this.exporter.setEdgeIdProvider(dependency -> dependency.getId());

        this.importer = new JSONImporter<>();
        this.importer.setVertexWithAttributesFactory((id,attributes) -> {
            Artifact artifact = null;
            if (attributes.containsKey("version")) {
                artifact = new VersionedArtifact();
                ((VersionedArtifact)artifact).setVersion(attributes.get("version").getValue());
            }
            else {
                artifact = new Artifact();
            }
            artifact.setGroupId(attributes.get("groupId").getValue());
            artifact.setArtifactId(attributes.get("artifactId").getValue());
            return (T) artifact;
        });

        this.importer.setEdgeWithAttributesFactory((attributes) -> {
            Dependency dependency = new Dependency();
            dependency.setScope(attributes.get("scope").getValue());
            dependency.setSourceId(attributes.get("sourceId").getValue());
            dependency.setDestinationId(attributes.get("destinationId").getValue());
            return dependency;
        });


    }

    public void exportDependencyGraph (Graph<T,Dependency> graph, File file) {
        this.exporter.exportGraph(graph,file);
    }

    public void exportDependencyGraph (Graph<T,Dependency> graph, Writer writer) {
        this.exporter.exportGraph(graph,writer);
    }

    public Graph importDependencyGraph (File file) {
        Graph<T,Dependency> graph = new DefaultDirectedGraph<T, Dependency>(Dependency.class);
        this.importer.importGraph(graph,file);
        return graph;
    }

    public Graph importDependencyGraph (Reader reader) {
        Graph<T,Dependency> graph = new DefaultDirectedGraph<T, Dependency>(Dependency.class);
        this.importer.importGraph(graph,reader);
        return graph;
    }

}
