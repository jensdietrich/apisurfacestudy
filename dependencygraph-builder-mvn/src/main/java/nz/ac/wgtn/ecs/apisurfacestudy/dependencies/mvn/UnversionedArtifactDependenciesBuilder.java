package nz.ac.wgtn.ecs.apisurfacestudy.dependencies.mvn;


import java.util.function.Predicate;

public class UnversionedArtifactDependenciesBuilder extends  AbstractArtifactDependencyGraphBuilder<Artifact> {


    public UnversionedArtifactDependenciesBuilder() {
    }

    public UnversionedArtifactDependenciesBuilder(Predicate<String> scopeFilter) {
        super(scopeFilter);
    }

    @Override
    protected Artifact createNewArtifact(String groupId, String artifactId, String version) {
        return new Artifact(groupId,artifactId);
    }
}
