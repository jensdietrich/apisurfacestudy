package nz.ac.wgtn.ecs.apisurfacestudy.dependencies.mvn;

import org.jgrapht.Graph;
import org.jgrapht.nio.Attribute;
import org.jgrapht.nio.DefaultAttribute;
import org.jgrapht.nio.dot.DOTExporter;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Utility to export a dependency graph to DOT for visualisation.
 * @author jens dietrich
 */
public class DependencyGraph2Dot {


     public static <T extends Artifact> void exportDependencyGraph (Graph<T,Dependency> graph, File file, boolean showScopesAsEdgeLabels) {
        DOTExporter<T,Dependency> exporter = null;
        exporter = new DOTExporter<>();
        // exporter.setVertexAttributeProvider(artifact -> artifact.getAttributes());
        exporter.setVertexIdProvider(artifact -> '\"' + artifact.getId() + '\"');
        if (showScopesAsEdgeLabels) {
           exporter.setEdgeAttributeProvider(dependency -> {
              Map<String, Attribute> attributes = new HashMap<>();
              attributes.put("label", DefaultAttribute.createAttribute(dependency.getScope()));
              return attributes;
           });
        }
        exporter.setEdgeIdProvider(dependency -> "foo");
        exporter.exportGraph(graph,file);
    }

}
