package nz.ac.wgtn.ecs.apisurfacestudy.dependencies.mvn;

import org.jgrapht.nio.Attribute;
import org.jgrapht.nio.DefaultAttribute;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Minimalistic representation of an artifact.
 * @author jens dietrich
 */
public class Artifact {
    private String groupId = null;
    private String artifactId = null;

    public Artifact() {}

    public Artifact(@Nonnull String groupId, @Nonnull String artifactId) {
        this.groupId = groupId;
        this.artifactId = artifactId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getArtifactId() {
        return artifactId;
    }

    public void setArtifactId(String artifactId) {
        this.artifactId = artifactId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Artifact artifact = (Artifact) o;
        return Objects.equals(groupId, artifact.groupId) &&
                Objects.equals(artifactId, artifact.artifactId);
    }

    // for persistency
    public Map<String, Attribute> getAttributes() {
        Map<String, Attribute> attributes = new HashMap<>();
        attributes.put("artifactId", DefaultAttribute.createAttribute(this.artifactId));
        attributes.put("groupId", DefaultAttribute.createAttribute(this.groupId));
        return attributes;
    }
    public String getId() {
        return groupId + ':' + artifactId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(groupId, artifactId);
    }

    @Override
    public String toString() {
        return "Artifact{" + groupId + ':' + artifactId + '}';
    }
}
