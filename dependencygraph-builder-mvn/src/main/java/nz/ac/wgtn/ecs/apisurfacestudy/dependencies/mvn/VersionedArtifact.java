package nz.ac.wgtn.ecs.apisurfacestudy.dependencies.mvn;

import org.jgrapht.nio.Attribute;
import org.jgrapht.nio.DefaultAttribute;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Minimalistic representation of a versioned artifact.
 * @author jens dietrich
 */
public class VersionedArtifact extends Artifact {
    private String version = null ; // actually, a version constrain

    public VersionedArtifact() {}

    public VersionedArtifact(@Nonnull String groupId, @Nonnull String artifactId, @Nonnull  String version) {
        super(groupId, artifactId);
        this.version = version;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    // for persistency

    @Override
    public Map<String, Attribute> getAttributes() {
        Map<String, Attribute> attributes = super.getAttributes();
        attributes.put("version",DefaultAttribute.createAttribute(this.getVersion()));
        return attributes;
    }
    @Override
    public String getId() {
        return super.getId() + ':' + this.version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        VersionedArtifact that = (VersionedArtifact) o;
        return Objects.equals(version, that.version);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), version);
    }

    @Override
    public String toString() {
        return "Artifact{" + getGroupId() + ':' + getArtifactId()+ ':' + version + '}';
    }
}

