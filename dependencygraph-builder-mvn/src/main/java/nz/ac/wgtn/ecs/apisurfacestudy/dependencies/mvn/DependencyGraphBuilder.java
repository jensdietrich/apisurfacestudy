package nz.ac.wgtn.ecs.apisurfacestudy.dependencies.mvn;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.jgrapht.Graph;
import org.zeroturnaround.exec.ProcessExecutor;
import org.zeroturnaround.exec.ProcessResult;
import java.io.File;
import java.util.Collection;
import java.util.List;

public class DependencyGraphBuilder {

    public static void main (String[] args) throws Exception {

        LoggerContext logContext = (LoggerContext) LogManager.getContext(false);
        Collection<Logger> allLoggers = logContext.getLoggers();

        for (Logger logger:allLoggers) {
            System.out.println(logger);
        }

        File folder = new File(System.getProperty("user.dir"));
        Preconditions.checkState(folder.exists());
        File pom = new File(folder,"pom.xml");
        Preconditions.checkState(pom.exists());

        // run mvn dependency:tree
        ProcessResult result = new ProcessExecutor()
                .directory(folder)
                .readOutput(true)
                .command("mvn","dependency:tree")
                .execute();
        String output = result.outputString();


        // TODO chose format, write to file





    }

    static <T extends Artifact> Graph<T,Dependency> parse(String depTreeoutput,DependencyTreeParserListener parserListener) {
        List<String> lines = Lists.newArrayList(depTreeoutput.split(System.lineSeparator()));
        return parse(lines,parserListener);
    }

    static <T extends Artifact> Graph<T,Dependency> parse(List<String> depTreeoutputLines,DependencyTreeParserListener parserListener) {

        boolean inTree = false;
        for (String line:depTreeoutputLines) {
            if (line.startsWith("[INFO] --- maven-dependency-plugin:")) {
                inTree = true;
            }
            else if (inTree) {
                if (line.startsWith("[INFO] ---")) {
                    inTree = false;
                }
                else {
                    int offset = countOffset (line);
                    String body = removePrefix(line);
                    String[] parts = body.split(":");
                    if (line.contains("+-")) {
                        assert parts.length==5;
                        parserListener.childArtifact(parts[0],parts[1],parts[3],parts[4],offset);
                    }
                    else if (line.contains("\\-")) {
                        assert parts.length==5;
                        parserListener.lastChildArtifact(parts[0],parts[1],parts[3],parts[4],offset);
                    }
                    else {
                        assert parts.length==4;
                        parserListener.rootArtifact(parts[0],parts[1],parts[3]);
                    }
                }
            }
        }

        return parserListener.buildGraph();
    }

    private static int countOffset(String line) {
        String core = removePrefix(line);
        int pos = line.indexOf(core);
        assert pos >= 7; // "[INFO] "
        pos = pos-7;
        assert pos%3 == 0; // offset used
        return pos/3;
    }

    private static String removePrefix(String line) {
       line = line.trim();
       if (line.startsWith("|")) {
           return removePrefix(line.substring(1));
       }
       else if (line.startsWith("[INFO]")) {
           return removePrefix(line.substring(6));
       }
       else if (line.startsWith("+-")) {
           return removePrefix(line.substring(2));
       }
       else if (line.startsWith("\\-")) {
           return removePrefix(line.substring(2));
       }
       else {
           return line;
       }
    }
}
