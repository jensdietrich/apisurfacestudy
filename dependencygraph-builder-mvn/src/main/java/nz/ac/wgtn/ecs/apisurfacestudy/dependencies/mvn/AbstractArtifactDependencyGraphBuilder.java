package nz.ac.wgtn.ecs.apisurfacestudy.dependencies.mvn;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultDirectedGraph;

import java.util.Stack;
import java.util.function.Predicate;

public abstract class AbstractArtifactDependencyGraphBuilder<T extends Artifact> extends  DependencyTreeParserListener {

    private Graph<T, Dependency> graph = new DefaultDirectedGraph<T, Dependency>(Dependency.class);
    private Predicate<String> scopeFilter = scope -> true;
    private Stack<T> stack = new Stack<>();
    private T lastArtifact = null;

    public AbstractArtifactDependencyGraphBuilder() {
    }

    public AbstractArtifactDependencyGraphBuilder(Predicate<String> scopeFilter) {
        this.scopeFilter = scopeFilter;
    }


    protected abstract T createNewArtifact(String groupId, String artifactId, String version);

    @Override
    public void rootArtifact(String groupId, String artifactId, String version) {
        T artifact = createNewArtifact(groupId,artifactId,version);
        graph.addVertex(artifact);
        stack.push(artifact);
    }

    @Override
    public void childArtifact(String groupId, String artifactId, String version, String scope,int depth) {
        if (scopeFilter.test(scope)) {
            T child = createNewArtifact(groupId,artifactId,version);
            maintainStack(depth);

            T parent = stack.peek();
            graph.addVertex(child);
            graph.addEdge(parent,child,new Dependency(parent.getId(),child.getId(),scope));
            lastArtifact = child;
        }
    }

    @Override
    public void lastChildArtifact(String groupId, String artifactId, String version, String scope,int depth) {
        if (scopeFilter.test(scope)) {
            T child = createNewArtifact(groupId,artifactId,version);

            maintainStack(depth);

            T parent = stack.peek();
            graph.addVertex(child);
            graph.addEdge(parent,child,new Dependency(parent.getId(),child.getId(),scope));
            lastArtifact = child;
        }
    }

    private void maintainStack(int depth) {
        assert stack.size()>0;
        if (stack.size()<depth) {
            stack.push(lastArtifact);
        }
        else if (stack.size()>depth) {
            stack.pop();
        }
        assert stack.size()==depth;
    }

    @Override
    public Graph<T, Dependency> buildGraph() {
        return graph;
    }
}
