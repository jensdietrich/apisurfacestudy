package nz.ac.wgtn.ecs.apisurfacestudy.dependencies.mvn;

import org.jgrapht.Graph;

/**
 * Listener for events that occur when parsing mvn dependency:tree console output.
 * @author jens dietrich
 */
public abstract class DependencyTreeParserListener <T extends Artifact>{

    public abstract void rootArtifact (String groupId, String artifactId, String version) ;

    public abstract void childArtifact (String groupId, String artifactId, String version,String scope,int depth) ;

    public abstract void lastChildArtifact (String groupId, String artifactId, String version,String scope, int depth) ;

    public abstract Graph<T, Dependency> buildGraph ();

}
