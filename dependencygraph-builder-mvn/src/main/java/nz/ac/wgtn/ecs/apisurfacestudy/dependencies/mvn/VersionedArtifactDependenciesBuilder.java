package nz.ac.wgtn.ecs.apisurfacestudy.dependencies.mvn;


import java.util.function.Predicate;

public class VersionedArtifactDependenciesBuilder extends  AbstractArtifactDependencyGraphBuilder<VersionedArtifact> {


    public VersionedArtifactDependenciesBuilder() {
    }

    public VersionedArtifactDependenciesBuilder(Predicate<String> scopeFilter) {
        super(scopeFilter);
    }

    @Override
    protected VersionedArtifact createNewArtifact(String groupId, String artifactId, String version) {
        return new VersionedArtifact(groupId,artifactId,version);
    }
}
